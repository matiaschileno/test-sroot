import axios from 'axios';

// change to http://localhost:3000 when backend is running locally
axios.defaults.baseURL = 'https://test-sroot-backend.plaza.ninja';

const AUTH_URL = '/auth';
const BANDWIDTH_URL = '/bandwidth';
const AUDIENCE_URL = '/audience';
// fixed user
const id = 'swagtv';
const pass = 'bling$bling';

let token = '';

export default {
  async getBandwidthData(from, to, aggregate) {
    const body = aggregate ? {
      from, to, session_token: token, aggregate,
    } : { from, to, session_token: token };
    const response = await axios.post(`${BANDWIDTH_URL}`, body);
    return response.data;
  },
  async getAudienceData(from, to, aggregate) {
    const body = aggregate ? {
      from, to, session_token: token, aggregate,
    } : { from, to, session_token: token };
    const response = await axios.post(`${AUDIENCE_URL}`, body);
    return response.data;
  },
  async auth(identifiant = id, password = pass) {
    const response = await axios.post(`${AUTH_URL}`, { identifiant, password });
    token = response.data.session_token;
    return response.data;
  },
};
