export default {
  months: [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec',
  ],
  getShortStringDate(dateInMilli) {
    const date = new Date(dateInMilli);
    const formattedDate = `${date.getDate()}. ${this.months[date.getMonth()]}`;
    return formattedDate;
  },
};
