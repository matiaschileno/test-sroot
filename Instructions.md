# Test FrontEnd
This test was made by Matías Plaza (matias.plaza.s@gmail.com)

This SPA was written in JavaScript using Vue.js 2

It was deployed using Docker with docker-compose.

## Use it
It has already been deployed and can be tested through the following link : https://test-sroot.plaza.ninja.

You can also run this frontend locally by:
- Running 'npm install' in /frontend' and 'frontend-test-backend'
- Launching backend by running 'node mock-backend.js' in /frontend-test-backend
- Changing Axios baseUrl by http://localhost:3000 in /frontend/src/api/requests.js
- Launching frontend by running 'npm run serve' in /frontend

## NOTE:
For simplicity sake, one user has been chosen by default, 'swagtv'. Because of this, a backend modification has been made (temporary for this test purpose). Request to /auth only permitted one session by user, so code was commented allowing re-authentification when a user is already logged in. This modification aims to allow to test this application from different devices without managing login/logout from frontEnd.

## Description
Everytime page is loaded, it will fetch audience and bandwidth data for this user.
In first load, start date will be the current datetime, and end date will be the start date - 15 days.
It is possible to change this range date in a DateTime picker in footer.




